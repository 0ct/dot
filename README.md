# 💻 dotfiles 💨💨

Usage
-------------------

```bash
$ bash init.sh
```

Supported types
-------------------
* vim
* zsh
* tmux
* git (installation only)
* fish
