#               ,______________________,
#               |****;/''   /*********/
#               |**7'      /*********/
#               |;(       /*********/
#                       /'********/'
#                      /*********/
#                    /'********/'
#                   /*********/       _.
#                 /'********/'       )*|
#                /*********/       ,7**|
#               /*********/______/*****|
#               """"""""""""""""""""""""
#               __              _
#               \ \      ___ __| |_
#                > >    |_ /(_-< ' \
#               /_/     /__|___/_||_|     ___
#                                        |___|

#       * auther    : kip-s
#       * url       : https://kip-s.net
#       * ver       : 0.10


if [ -e $ZDOTDIR/rc/zshrc ]; then
  source $ZDOTDIR/rc/zshrc
fi
